# Register user

The first thing you need to do to interact with pantahub is to register a user account. A user account gives you access to the full API, including the object store, and also grants you access to the dashboard on www.pantahub.com.

Register your user with the following [pvr](install-pvr.md) command:

```pvr register -u youruser -p yourpassword -e your@email.tld```

This will generate a json response with the server-generated part of the credentials:

```
2017/06/19 11:08:43 Registration Response: {
  "id": "5947949b85188a000c143c2e",
  "type": "USER",
  "email": "your@email.tld",
  "nick": "youruser",
  "prn": "prn:::accounts:/5947949b85188a000c143c2e",
  "password": "yourpassword",
  "time-created": "2017-06-19T09:08:43.767224118Z",
  "time-modified": "2017-06-19T09:08:43.767224118Z"
}
```

You can also go to http://www.pantahub.com and follow the sign-up process on the web interface.

![](images/ph-welcome.png)

Your account is not ready for use until you have followed email verification.
