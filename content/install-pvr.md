# Install pvr

Our main development tool is called ```pvr``` and it lets you interact with your Pantavisor-enabled device remotely through Pantahub. This CLI will be what you use to ```clone``` your device as well as ```post``` different demos to it for easy testing and seamless onboarding, among other operations. You can download it from the following locations:

## Get pvr stable

You can use docker to get the last stable version:

```docker run --rm registry.gitlab.com/pantacor/pvr:stable | tar x```

This will create a folder named ```bin``` with the ```pvr``` binary in subfolders for all supported platforms.

## Install the pvr binary

### Linux

To install the Linux version you need to place the extracted binary in your ```$PATH``` and make sure it is made executable with ```chmod +x```:

```
mkdir ~/bin
cp bin/<host-architecture>/pvr ~/bin/pvr
PATH=$PATH:<absolute-path-bin-pvr>
```

### Windows

To install the Windows version all you need is to place it in a directory to which your user has access and can run executables from. ```C:\Users\YOURUSER``` is usually a good location.

## Test pvr

Once you have it installed just calling the ```pvr``` command from your shell should show you the help menu, where you can get familiarized with the different features that it provides.

You can also take a look at the [maintain your system how-to guide](clone-your-system.md) for a view on how to operate over a Pantavisor device with ```pvr```.

## Get last pvr version

If you prefer to use the last ```pvr``` version instead of using the stable one, you can do the following:

```
pvr global-config DistributionTag=develop
pvr self-upgrade
```

The stored binary will automatically upgrade to the last version.
