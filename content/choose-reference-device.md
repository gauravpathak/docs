# Choose your Reference Device

To produce an image you will need the Pantavisor source code that you retrieved above as well as a Pantavisor reference device from Pantahub that you want to use to derive the system image from.

By default, the build system will generate an image that has no containers (therefore no functionality, including no rootfs). For instance, if you want to produce an image that has the same stack of one of your online devices, you would use the pvr clone URL of that device as a parameter to build pantavisor.

If you do not have your own custom device developed yet, you can use any of our initial devices as your starting point, e.g.

```
https://pvr.pantahub.com/pantahub-ci/malta_initial_latest
```

You can get this and other reference devices in our [download page](download-images.md), in the ```Pantahub devices``` column of the table.

Bear in mind that you will have to choose one that matches the architecture of your device. Also, note down the target value of your initial device. In the case of the previous example, it would be:

```
malta-qemu
```

If you rather want to use one of the devices you have developed and burn an image from it, you would go to your device details page on https://www.pantahub.com and get the ```pvr clone URL``` from there:

![](images/pantahub-device-details-1.png)
