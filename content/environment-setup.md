# Environment setup

**IMPORTANT:** These instructions are meant for any apt based system such as Ubuntu, but can be adapted for your favorite distro.

In order to use our tools, you need to setup your building and testing environment. The following packages are needed for building:

## curl

```
sudo apt update
sudo apt install curl
```

Try ```curl --help``` to check curl is installed.

## squahsfs

```
sudo apt update
sudo apt install squashfs-tools
```

Try ```mksquashfs --help``` to check squashfs is installed.

## git

```
sudo apt update
sudo apt install git
```

Before you use git, remember to configure your name and email globally:

```
git config --global user.name "Your Name"
git config --global user.email "your@email.tld"
```

## repo

We use the [repo](https://source.android.com/source/using-repo) tool used by Android project to maintain our source distribution for pantavisor.

To install ```repo```:

```
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
export PATH=~/bin/:$PATH
```

If you run into any problem with repo installation, further documentation can be found in the [Android Project](https://source.android.com/setup/build/downloading#installing-repo).

## docker

```
sudo apt update
sudo apt install docker.io
```

Allow managing docker as a non-root user:

```
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

After installing docker, check that you can actually use docker echo container using your normal user:

```
docker run hello-world
```

Expected output of that command should be similar to:

```
docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete 
Digest: sha256:41a65640635299bab090f783209c1e3a3f11934cf7756b09cb2f1e02147c6ed8
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

If that fails, find instructions on how to setup docker online.
