# Lime SDR initial devices

**IMPORTANT**: This section requires a Lime SDR micro USB radio on a Pantavisor device. To set up a Pantavisor device on Raspberry Pi 3 B+ with pre-compiled images, you can use our initial device image from [here](get-started.md).

Once you have a claimed Pantavisor device, you can connect the Lime SDR micro USB to the board, go to your Pantahub device board, and [clone](clone-your-system.md) the device to your computer. This creates a device representation in your host computer, which you can modify, [commit](make-a-new-revision.md), and [post](deploy-a-new-revision.md) to Pantahub. After that, the device will download the new revision and reset.

We have set up a series of Lime SDR Pantavisor applications that you can use and test over your Pantavisor device. You can add these with the [app add](pvr-docker-apps-experience.md) command. Have in mind that only one of these apps can be run at the same time in your board, as they will take control of the Lime SDR micro. In order to delete a Pantavisor app to make room for another one, just remove the app folder in your device checkout. After that, don't forget to ```add```, ```commit``` and ```post``` your change.

These are the pantavisor apps with the ```app add``` command to install them in your device. Notice these commands are for ARM architecture, so change to your architecture to fit your needs. You may have to check if this is possible in the link of each app:

| Name | Source | pvr Command |
| ---- | ------ | ----------- |
| limeScan | [link](https://gitlab.com/myriadrf/pantahub/limescan-device) | pvr app add --from registry.gitlab.com/myriadrf/pantahub/limescan-device:ARM32V6 limescan |
| osmocom | [link](https://gitlab.com/myriadrf/pantahub/osmo-gsm-net) | pvr app add --from registry.gitlab.com/myriadrf/pantahub/osmo-gsm-net:ARMHF-master osmocom |
| limeSNA | [link](https://gitlab.com/myriadrf/pantahub/limeSNA) | pvr app add --from registry.gitlab.com/myriadrf/pantahub/limesna:ARM32V6-docker limesna |
| SDRangel | [link](https://gitlab.com/myriadrf/pantahub/sdrangel) | pvr app add --from registry.gitlab.com/myriadrf/pantahub/sdrangel:ARM32V7-ubuntu sdrangel |
