# Push App Logs to Pantahub

By default logs are enabled for the apps startup and console. Pantavisor allows you to enable logs from within the apps. These logs are pushed to Pantahub as they're generated.

Logs are classified in two types:

* LXC (App startup) logs and console logs. Enabled by default.
* Logs from within app. For example: syslogd.

If you want to modify the default configuration for LXC and console logs, or push an app's log (syslogd or other application generated one), some additions are required in the app's run.json [logs object](pantavisor-configuration.md#logs`).

For this example, we have an app named alpine-nginx comprised of an Alpine rootfs with a nginx server installed in it. We want to enable syslog and ngingx log. We will also change the configuration for lxc log to increase its stored size to 4 MiB. 

Let us first check how alpine-nginx/run.json looks by default in the checkout of our cloned device:

```
{
    "#spec": "service-manifest-run@1",
    "config": "lxc.container.conf",
    "name":"alpine-hotspot",
    "storage":{
        "lxc-overlay" : {
            "persistence": "boot"
        }
    },
    "type":"lxc",
    "root-volume": "root.squashfs",
    "volumes":[]
}
```

To make the changes described above, we would have to add this ```logs``` object:

```
{
    "#spec": "service-manifest-run@1",
    "config": "lxc.container.conf",
    "name":"alpine-hotspot",
    "storage":{
        "lxc-overlay" : {
            "persistence": "boot"
        }
    },
    "type":"lxc",
    "root-volume": "root.squashfs",
    "volumes":[],
    "logs": [
        {“file”:”/var/log/syslog”,”maxsize”:102485760,”truncate”:true,”name”:”alpine-logger”},
        {“file”:”/www/log/wwwlog”,”maxsize”:102485760,”truncate”:true,”name”:”nginx-logger”}
		{“lxc”:"enable","maxsize":409943040,"truncate":true,"name":"alpine-lxc"}
    ]
}
```


