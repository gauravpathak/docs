# Build Individual Components

You can also build individual components of the system by calling the submodule directly as secondary target:

```BUILD=./build.docker.sh```

This builds the Pantavisor binary and all its dependencies:
```$BUILD malta-qemu init```

This builds the pv_lxc container runtime plugin for Pantavisor:
```$BUILD malta-qemu pv_lxc```

This assembles the initrd image '0base.cpio.xz' from all build assets:
```$BUILD malta-qemu image```

You can also clean individual components by appending ```-clean``` to the build subtarget:
```$BUILD malta-qemu init-clean```

This runs the 'trail' assembly step, which results in the final PV-trail for storage:
```$BUILD malta-qemu trail```


