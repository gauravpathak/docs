# First boot

This section is aimed on users with no experience on Raspberry Pi 3 boot up process.

Before starting up your board, you have to connect it to an internet-facing network via the Ethernet port, insert your SD card and power up the board. This process is described following the steps below:

## Set up LAN connection to your board via Ethernet

You can set up your connection either by connecting your board to your local network (recommended) or connecting it to your computer.

![](images/lan-connection.png)

In order to enable internet sharing through Ethernet from your computer, follow your OS instructions:

### Linux

TODO

### Mac OS

1. Open System Preferences
2. Select Sharing Preference Pane
3. Enable Internet Sharing with the following settings:
   * Share your connection from: Wi-Fi
   * To computers using: Ethernet

![](images/enable-internet-sharing.png)

## Insert SD card into the board

![](images/insert-sd-card.png)

## Supply power to your board

You can simply do that by connecting a USB micro cable between your computer and your board:

![](images/supply-power.png)

If everyhing went well, the PWR led (red) should now turn on, while the ACT led (green) will follow blinking. After the kernel is loaded, ethernet led (orange) should start blinking too. That would mean the board is up and LAN conection is on.
