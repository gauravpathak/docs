# Get started

This tutorial is meant to get you started on Pantavisor and Pantahub set up and development using ready-to-use Raspberry Pi pre-compiled images.

**IMPORTANT**: To follow this guide, you will need a Raspberry Pi 3 B+ with Ethernet connection to an internet-facing network. If you don't have one, you can also check our how use other pre-compiled images [here](choose-image.md) for other targets, such as Malta QEMU, or build your own [here](get-source-code.md).

Assuming you have already gone through our [prerequisites](environment-setup.md) section, we are going to get us started with these three simple steps:

1. [Image setup](image-setup-rpi3.md): download and flash our pre-compiled Pantavisor image on a SD card.
2. [First boot](first-boot-rpi3.md): insert the SD card in the RPi3 slot and boot it up.
3. [Claim your device](claim-device-rpi3.md): claim your running Pantavisor device on Pantahub.

After this, you will be able to remotely manage your newly claimed device using Pantahub. Check out our how-to guide on [maintaining](clone-your-system.md) your device for further instructions on how to develop and deploy your product.
