# Set Device Metadata

Device metadata can be usefull for communication with the device through Pantahub, and allows you do things like store SSH keys, auto-follow other devices, inspect the device network address...

Check the [metadata reference](pantavisor-metadata.md) to see which values can be set by default in a Pantavisor device.

### Send a Metadata Pair

**IMPORTANT**: The following instructions assume you already have claimed your device in pantahub.com.

Go to your pantahub.com dashboard:

![](images/dashboard.png)

Go to your devices:

![](images/device-list.png)

Click on your newly claimed device in your device list. In the tab ```metadata```, you can create a new pair in ```user-meta```:

![](images/user-meta.png)
