# Download instructions


Here, you can download our base pantavisor image for your device. For instructions on how to install one of these Pantavisor images, see our [how-to guide](choose-image.md)




## Stable


 - Release: [010](https://gitlab.com/pantacor/pv-manifest/-/tags/010)
 - Date: January 14, 2020
 - Summary: Second automatically generated stable version
 - Dependencies: [009-90-g7a1f83bd](https://gitlab.com/pantacor/pvr/commit/7a1f83bd8d20f1a8d2a06a7171a8d2c082e650eb)


**Downloads:**

 | Platform | Target | Flashable images | Pantahub devices | 
 | --- | --- | --- | --- | 
 | **bpi-r2** | arm-bpi-r2 | [download](https://pantavisor-ci.s3.amazonaws.com/tags/010/109276528/bpi-r2/bpi-r2.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d64df359061a500090ea599/step/117)
**malta** | malta-qemu | [download](https://pantavisor-ci.s3.amazonaws.com/tags/010/109276528/malta/malta.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d553ba8e8c4940008979089/step/3)
**rpi3** | arm-rpi3 | [download](https://pantavisor-ci.s3.amazonaws.com/tags/010/109276528/rpi3/rpi3.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d4adf5a37d394000807f5ee/step/4)
**rpi4** | arm-rpi4 | [download](https://pantavisor-ci.s3.amazonaws.com/tags/010/109276528/rpi4/rpi4.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ddbf18787f1d100082dce13/step/44)
**x64-uefi-installer** | x64-uefi | [download](https://pantavisor-ci.s3.amazonaws.com/tags/010/109276528/x64-uefi-installer/x64-uefi-installer.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d838a31f99f9c00095b2f21/step/101)
**x64-uefi** | x64-uefi | [download](https://pantavisor-ci.s3.amazonaws.com/tags/010/109276528/x64-uefi/x64-uefi.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d838a31f99f9c00095b2f21/step/101) | 


**Release notes:**


 - New PANTAVISOR_DEBUG build option enables telnet, ssh and tty debug shell
 - User can now access via ssh to all apps with the newly integrated dropbear server in pantavisor setting the PANTAVISOR_DEBUG build option
 - Telnet now uses sh instead of ash
 - Allow TPM encryption with the PANTAVISOR_DISKCRYPT build option
 - Enablement of log configuration that allows to decide which app logs must be tracked and pushed to pantahub
 - Enablement of app console log tracking by default
 - Better system calls with exit code and log support from pantavisor
 - Support for LABEL or UUID instead of specifying the actual device
 - Metadata now persists across reboots
 - Cleanup debug messages
 - Fixed several parsing problems of the configuration json files
 - Fixed device status sent to pantahub when correctly updated
 - Fixed a bug that made the device not rebooting when pantavisor ended unexpectedly
 - Fixed waiting time for usb device enumeration for slow devices

## Latest


 - Release: [010-55-g46e1a71](https://gitlab.com/pantacor/pv-manifest/commit/46e1a71dd4a07527752b88ec9811fa9ade3be148)
 - Date: April 1, 2020
 - PVR version: 009-108-g6c6dc4d7


**Downloads:**

 | Platform | Target | Flashable images | Pantahub devices | 
 | --- | --- | --- | --- | 
 | **arm-generic** | arm-generic |   | [device](https://www.pantahub.com/u/pantahub-ci/devices/5e3aaa97628350000a207375/step/32)
**bpi-r2** | arm-bpi-r2 | [download](https://pantavisor-ci.s3.amazonaws.com/branches/master/131629090/bpi-r2/bpi-r2.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d64df359061a500090ea599/step/160)
**malta** | malta-qemu | [download](https://pantavisor-ci.s3.amazonaws.com/branches/master/131629090/malta/malta.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d553b5ae8c4940008978a8e/step/176)
**mips-generic** | mips-generic |   | [device](https://www.pantahub.com/u/pantahub-ci/devices/5e394cd40c798800091dd390/step/31)
**rpi0w** | arm-rpi0w | [download](https://pantavisor-ci.s3.amazonaws.com/branches/master/131629090/rpi0w/rpi0w.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5e5cea9c745f14000a4221ea/step/26)
**rpi3** | arm-rpi3 | [download](https://pantavisor-ci.s3.amazonaws.com/branches/master/131629090/rpi3/rpi3.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d35db3c4db15e0008fef281/step/287)
**rpi4** | arm-rpi4 | [download](https://pantavisor-ci.s3.amazonaws.com/branches/master/131629090/rpi4/rpi4.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ddbf18787f1d100082dce13/step/90)
**x64-uefi-installer** | x64-uefi | [download](https://pantavisor-ci.s3.amazonaws.com/branches/master/131629090/x64-uefi-installer/x64-uefi-installer.img.xz) |  
**x64-uefi** | x64-uefi | [download](https://pantavisor-ci.s3.amazonaws.com/branches/master/131629090/x64-uefi/x64-uefi.img.xz) | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d838a31f99f9c00095b2f21/step/145) | 


You have a complete list of our daily builds [here](pantavisor-ci-table.md).

