
Once you have done some changes like described in previous section, you
can deploy the currently staged pvr state to any device for which you
have owner permissions.

For that use ```pvr post``` command.

```pvr post``` has an optional argument where you can specify a pvr clone
URL for the device you want the currently staged system state to be posted
to.

Once post has ben submitted to a device, the device will eventually wake up
and try to consume the new state. On success, you will see the device going to
**```DONE```** state. If not, it will go to **```ERROR```** state.

