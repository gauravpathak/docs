# Claim your Device

There are three ways to claim your device, depending on the capabilities of the target image you just installed: manually from the host, manually from the target and automatically.

## Claim your Device manually from the Host

This section describes how to manually claim your device using ```pvr```. The device must have Avahi discoverability features, which can be enabled with our pv-avahi app (container), included in some of our initial images.

**IMPORTANT**: It is assumed that the board is connected to an internet-facing network. It is also assumed that your computer is in the same local network as your board.

```
pvr scan
```

With the ```pvr scan``` command, we will be able to discover devices in our network, both claimed and unclaimed, if they have no owner:

![](images/unclaimed-pvr-windows.png)

If ```pvr``` finds a device on the local network, it will present this metadata for you to claim it via the command quoted in the output. Just run that command as-is and your user should now own your device.

## Claim your Device manually from the Target

This section describes how to manually claim your device from the target, which could prove useful for devices that don't have Avahi discoverability features, like in the case of our Malta QEMU initial image.

**IMPORTANT**: It is assumed that the board is connected to an internet-facing network. It is also assumed that you have access to your device console.

In this case we need the challenge and the device-id of the temporary device so that it can be claimed by the just-registered user account. In the device console:

```
# cat /pantavisor/challenge
pleasantly-finer-unicorn
# cat /pantavisor/device-id
5b582638c67920b9de2
```

We can then use the ```pvr``` tool to claim with the user account. Please note that the last argument is the device endpoint, in full URL form, to the Pantahub host where this device exists.

**IMPORTANT**: Remember to use the device-id and challenge from above, not the example, in all ```pvr``` commands.

```
pvr claim -c CHALLENGE https://api.pantahub.com/devices/DEVICE-ID
```
```
*** Login (/type [R] to register) @ https://api.pantahub.com/auth (realm=pantahub services) ***
Username: youruser
Password: ********
```

You can also claim your device directly from the web interface of pantahub.com. Once it has been claimed and the device has realized this, it will show up in your devices list.

![](images/ph-claim.png)

## Claim your Device automatically

You can get advantage of our auto-claim feature if you are building your own image from source.

To build your image, you can go [here](get-source-code.md). Remember that you will need to use a special [building option](build-options.md#use-auto-claim-feature).

**IMPORTANT**: It is assumed that the board is connected to an internet-facing network.

With this, your device will be auto-claimed from your account after the first boot up.
