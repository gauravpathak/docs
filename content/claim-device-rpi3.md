# Claim your device

**IMPORTANT**: It is assumed that the Raspberry Pi is connected to an internet-facing network via the Ethernet port. It is also assumed that your computer is in the same local network as your board.

In order to claim your device you will need a Pantahub account, so make sure you have [created](register-user.md) one and have had it approved before the following. We are mostly interested now in the following command:

```pvr scan```

With the ```pvr scan``` command, we will be able to discover devices in our network, both claimed and unclaimed, if they have no owner. We must begin by discovering and claiming our device:

![](images/unclaimed-pvr-windows.png)

If `pvr` finds a device on the local network it will present this metadata for you to claim it via the command quoted in the output. Just run that command as-is and your user should now own your device.

If no devices are detected with the ```pvr scan```, go to our [troubleshooting](board-troubleshooting-rpi3.md) section.

## Notes: How does this work?

On first boot Pantavisor will start the Alpine app and wait for it to configure the network devices until pantahub.com is reachable. Once this happens, Pantavisor will proceed to register the newly seen device with pantahub.com and issue a challenge for the device to be claimed by a registered user account.

It can take a minute or two, depending on your connection, for a device to fully show up on Pantahub's device list the first time it boots. Wait a few if it hasn't showed up, it eventually will. If everything goes well this means your device has been claimed by the user account.

You can now use pvr to [clone](clone-your-system.md) your device, work on it's [state](make-a-new-revision.md) and [push](deploy-a-new-revision.md) a new revision.
