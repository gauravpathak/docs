# Get the source code

**IMPORTANT:** This guide focuses on building Pantavisor from source code. You can get pre-compiled Pantavisor images for all supported targets in our [download](download-images.md) page.

This build system is based on the Alchemy tool, plus modifications specific to the requirements of Pantavisor-enabled images. Make a new workspace directory:

```
mkdir pantavisor
cd pantavisor
```

To initialize it, use [`repo`](https://source.android.com/source/using-repo):

```
repo init -u https://gitlab.com/pantacor/pv-manifest
repo sync -j10
```

Afterwards, you should have contents similar to below in our pantavisor directory:

```
alchemy  bootloader  build.docker.sh  build.sh  config
external  internal  kernel  out  scripts  toolchains  vendor
```
