# Raspberry Pi 3 troubleshooting

## board does not boot up

If the RPi3 ACT led (green) is not blinking, we may be facing some boot up problem.

To check if the flashing went well, you can turn off your RPi3 and retreive the SD card and insert it in your computer. The SD card should have two partitions, pvboot and pvroot. If not, go back to our [image setup](image-setup-rpi3.md) section.

In order to discard any hardware problems, you could try one of the Raspberri Pi initial images, for example, [NOOBS](https://www.raspberrypi.org/documentation/installation/noobs.md).

## board boots up but do not connect to LAN

If the RPi3 ACT led (green) is blinking, but the ethernet led (orange) is not, you can inspect the Linux Kernel log via TTY. TTY debugging will need a Pantavisor image built with the [debug](build-options.md) option as the one from this tutorial. We can use either HDMI (recommended) or TTY (if you want to use the console).

### HDMI

Connecting and HDMI cable to Raspberry before booting up should show the kernel log when starting up the board.

### TTY

You have to connect the Raspberry Pi board to the USB-to-TLL board (or similar) following this indications:

|Raspberry Pi Board | USB to TTL|
|-------------------|-----------|
|Pin 6 | GND|
|Pin 8 | RXD|
|Pin 10 | TXD|

![](images/pi-to-usb-ttl.png)

**IMPORTANT**: To Identify the Pins(6,8 & 10) on the board, please check below:

![](images/pin-board.png)

Then connect the USB-to-TTL board to your computer:

![](images/usb-ttl-to-laptop.png)

After this, these are the preparations you have to made in each OS before booting up the device.

#### Linux

You can just list serial lines with the dmesg command to identify which /dev/ttyX device is the one corresponding with your latest connected USB-to-TLL board. Then start minicom with said device as parameter:

```
dmesg | grep tty
sudo minicom /dev/ttyX
```

#### Mac OS

First, you need to install CP210x USB to UART Bridge VCP Drivers:

 * Download link: [CP210x USB to UART Bridge VCP Driver for Mac OS](https://www.silabs.com/documents/public/software/Mac_OSX_VCP_Driver.zip)

Verify the USB-to-TLL board is detected by your computer:

```
ls /dev/cu*
> cu.SLAB_USBtoUART
```

Now, to sniff what you get from the serial cable, you can use ```screen```

```
screen /dev/cu.SLAB_USBtoUART 115200 -L
```

You will see a blank terminal waiting for Raspberri Pi 3 input. It is time to turn it on.

![](images/screen.png)

## board connects to LAN but pvr scan does not show any device

If no devices are detected, go through this check-list:

* both your Raspberry Pi and your computer are connected to the same network
* mDNS packets are allowed in your router and port 5353 is open in your computer firewall
* access to your router admin settings and see if DHCP has served an IP to the board and that you can ping it
