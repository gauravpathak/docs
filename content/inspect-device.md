# Inspect your Device

This section explains how to connect to your Pantavisor device from your host computer in an easy way.

**IMPORTANT:**: The image will have to be built with the [PANTAVISOR_DEBUG](build-options.md#build-debug-system-images) option.

Before we start, we will have to create a ssh public key and set it up in our device metadata. In Linux, you can create a ssh pair and print your public key with these commands:

```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
cat ~/.ssh/id_rsa.pub
```

Now, create a new [metadata pair](set-device-metadata.md) for the [SSH public key](pantavisor-metadata.md#ssh-authorized-keys):

key: pvr-sdk.authorized_keys
value: <ssh pub id>

![](images/user-meta.png)

## SSH

**IMPORTANT**: It is assumed for all commands that ```10.0.0.1``` is the reachable IP address, however this might be different depending on what you get from your local network. You can check the assigned IPs for all network intefaces in your pantahub.com device's metadata. It is asumed also that the app to reach is alpine-hotspot, but this can be changed to any app running on the device.

SSH server (Dropbear) should be accessible at port 8222 and the desired app as user:

```
ssh -p 8222 alpine-hotspot@10.0.0.1
```

You can directly reach Pantavisor with the "/" user (root):

```
ssh -p 8222 /@10.0.0.1
```

## Telnet

Once we are inside one of the app, we can get to telend in Pantavisor:

```
telnet localhost
```

## lxc-console

If we are inside Pantavisor via SSH or telnet, we can access the different app using the lxc-console. To get a session in the alpine-hotspot app:

```
lxc-console -n alpine-hotspot
```
