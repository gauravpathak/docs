# Image setup

## Download initial image

In order to try out the Raspberry Pi 3 Model B+ examples you can install our base Pantavisor-enabled image. This image contains a small Alpine Linux based container that provides discovery services and basic connectivity so that our development tools can be used to work on your device. You can download the base image from:

 * Raspberry Pi 3 Model B+: [rpi3.img.xz](https://pantavisor-ci.s3.amazonaws.com/tags/010/109276528/rpi3/rpi3.img.xz)

You can check other versions of our base image [here](download-images.md).

## Flash initial image

You can install this image with your preferred tool. Specific instructions for your operating system can be found on the Raspberry Pi installation instructions for [Linux](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md), for [Mac OS](https://www.raspberrypi.org/documentation/installation/installing-images/mac.md) as well as for [Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md)

### Linux

For quick Linux instructions you can use the ```dd``` tool following these steps (remember to substitute /dev/sdX for the device node corresponding to your SD card):

```
unxz rpi3-pv-1024MiB.img.xz
umount /dev/sdX*
sudo dd if=rpi3-pv-1024MiB.img of=/dev/sdX bs=32M
sync
```

### Mac OS

For Mac Os, the procedure is similar. First step would be to manually extract the rpi3-pv-1024MiB.img.xz file. Then you could find the SD card device name by opening a terminal and running the following command:

```df -h```

![](images/df-h.png)

You can see that on the bottom a 15 GB disk /dev/disk3 is mounted on /Volimes/BOOT.

After that, you can unmount it and flash it with the ```dd``` tool (remember to substitute /Volumes/BOOT and /dev/disk3 for the device node corresponding to your SD card):

```diskutil umount /Volumes/BOOT```

![](images/unmount-sd-card.png)

```
sudo dd if=rpi3-pv-1024MiB.img of=/dev/disk3 bs=32m
sync
```

![](images/burn-image-to-sd-card.png)

You can verify the image contents by opening the SD card partition named "boot":

![](images/verify-sd-card.png)

## Notes: What does this image contain?

Any Pantavisor device must have a BSP (Linux Kernel, Pantavisor binary and Linux drivers) and one to many apps (containers).

In the case of the default RPi3 image, is running these three Linux apps:

 * **alpine-hostpot**: automatically brings up basic cabled networking and sets up a hotspot with SSID "pantademo-XXXXX" (XXXXX = last five digits of device's MAC address) and pass "pantacor".
 * **pv-avahi**: it uses DNS multicast for device discoverability.
 * **pvr-sdk**: contains tools to help your development and debugging.

The example image provided is running a few more things next to Pantavisor. For example, a ssh server for inspecting each one of the containers, including a telnetd that allows you to introspect the root mount namespace to get more familiar with the system internals, tty console support for boot up debugging...

This image has been generated using [this](https://www.pantahub.com/u/pantahub-ci/devices/5d4adf5a37d394000807f5ee/step/3) Pantahub device. More on how to do this [here](get-source-code.md).
