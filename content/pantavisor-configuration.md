# App Configuration

Pantavisor configuration at app level.

## run.json

### logs

Allows configuring the pushing of logs to Pantahub from each app. There are three types of pushable to Pantahub logs: app, LXC and console logs.

#### App logs

For pushing logs from within container to pantahub, the following json object is used under the logs object:

```
{
	“file”:,
	“maxsize”:,
	“truncate”:,
	“name”:;
}
```

The table below describes the properties of the json object above,

| Property | Descripton                                              |
| -------- |:------------------------------------------------------- |
| file     | Absolute location of the log file within the container. |
| maxsize  | max file size in bytes before truncating.               |
| truncate | true \| false                                           |
| name     | logger daemon name                                      |

One dedicated logger is created by Pantavisor for every such object found. For example:

```
"logs”:[
		{“file”:”/var/log/syslog”,”maxsize”:102485760,”truncate”:true,”name”:”alpine-logger”},
		{“file”:”/tmp/app.log”,”maxsize”:102485760,”truncate”:true,”name”:”app-logger”}
	]
```

####  LXC log

Enabled by default. Log truncation maxsize is 2MiB. The following json object is used under the logs object:

```
{
	“lxc”:,
	“maxsize”:,
	“truncate”:,
	“name”:
}
```

| Property | Descripton                                                       |
| -------- |:---------------------------------------------------------------- |
| lxc      | use keyword "enable" to push lxc startup logs to Pantahub. |
| maxsize  | max file size in bytes before truncating.                        |
| truncate | true \| false                                                    |
| name     | logger daemon name                                               |

May be added to run.json logs to change the default settings. For example, for disabling truncation:

```
"logs":[
		{“lxc”:"enable","maxsize":204971520,"truncate":false,"name":"alpine-lxc"},
    ]
```

#### Console log

Enabled by default. Log truncation maxsize is 2MiB. The following json object is used under the logs object:

```
{
	“console”:,
	“maxsize”:,
	“truncate”:,
	“name”:
}
```

| Property | Descripton                                                       |
| -------- |:---------------------------------------------------------------- |
| console  | use keyword "enable" to push lxc startup logs to Pantahub.|
| maxsize  | max file size in bytes before truncating.                        |
| truncate | true \| false                                                    |
| name     | logger daemon name                                               |

May be added to run.json logs to change the default settings. For example for changing truncation maxsize to 512KiB:

```
"logs":[
		{“console":"enable","maxsize":51242880,"truncate":true,"name":"alpine-console"}
    ]
```

### storage

Allows to change the persitence of an app rootfs. There are three types of persitence:

* permanent: changes are stored locally permanently. Not changing if a new revision is pushed or redeployed.
* revision: changes are pegged locally to the revision. A redeploy of a previous revision will recover the changes of that revision and delete the changes of the current one. A push of a new revision will not delete them.
* boot: changes are volatile like in any tmpfs. Any push or redeploy will delete the changes in the rootfs.

The following json object is used under the storage object to change persistence:

```
{
	“lxc-overlay”:{
        "persistence":
    }
}
```

| Property    | Descripton                                                       |
| ----------- |:---------------------------------------------------------------- |
| persistence | "permanent", "revision" and "boot"                               |

For example, if you want to change the app rootfs persistence to boot:

"storage":{
    "lxc-overlay":{
        "persistence":"boot"
    }
}
