# Image setup

The image has now to be flashed in different ways depending on the target.

## Targets

### RPi3, RPi4 or BPi-R2

To use any of our pre-compiled images for these targets, you will only have to flash your SD card and boot the board up. You can see how to do this in a detailed way in our [get started](get-started.md) guide.

### Malta

To try out our malta image on QEMU, you will first have to follow our [build process](get-source-code.md) for the malta-qemu target. After compiling, you will be able to either use your newly built image, or the one you downloaded from our page, with the following script:

```
sudo scripts/qemu.sh malta.img
```

This script allows for easy configuration of bridge networking and all that you need to quickly bring up a test environment.

On first boot Pantavisor will start the Malta LEDE platform and wait for it to configure the network devices until pantahub.com is reachable. Once this happens, Pantavisor will proceed to register the newly seen device with pantahub.com and issue a challenge for the device to be claimed by a registered user account.

When `qemu.sh` script ends, you can press ENTER to get the LEDE console.

### x64-uefi

To easily install this image, we provide the x64-uefi-installer image, which will have to be booted up from BIOS/UEFI. For this, you will need a keyboard and a screen.

First step will be to flash your storage device with the image, you can follow the same steps as in our [get started](image-setup-rpi3.md) guide.

After that, you will have to insert the storage device in your x64 device and boot it up. Access your BIOS/UEFI and boot the device from the storage device that contains the image.

After the prompt, enter the command:

```
pvf-install
```

The installer will automatically flash the image. Remove the storage device when prompted and boot the device up again.

## Troubleshooting

Our pre-compiled images are built with the [PANTAVISOR_DEBUG](build-options.md#build-debug-system-images) option, so you will have SSH, TTY and other goodies for debugging your devices. Therefore, most of the things explained in our get started [troubleshooting](board-troubleshooting-rpi3.md) are applicable here.
