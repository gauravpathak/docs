# Build pantavisor

For building, the flow is very similar to the Android build system, except that targets are not setup in advance of operations but rather these are conditional on the target and the flow is managed by scripts/build.sh.

Current supported targets are ```[arm-bpi-r2, malta-qemu, arm-rpi3, arm-rpi4, x64-uefi, x64-uefi-installer]``` and a few other test ones.

## Build your chosen reference device

After you have [downloaded](get-source-code.md) Pantavisor source code and [chosen](choose-reference-device.md) a reference device matching your device target architecture, you can now start the building process.

First of all, check you still are in your pantavisor workspace path. Output of ```ls``` should look similar to:

```
alchemy  bootloader  build.docker.sh  build.sh  config
external  internal  kernel  out  scripts  toolchains  vendor
```

Now, you can start the compilation of all the dependencies and flashable image with this command, changing the pvr clone URL and target architecture to your chosen ones:

```
PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/malta_initial_stable ./build.docker.sh malta-qemu
```

This will generate all dependencies of the malta-qemu build tree and build a final image according to the values defined in ```config/malta/image.config```. You can find the generated image in ```out/malta/flash-malta-16384.img```. This is a pflash QEMU bootable image to be used with the locally built host QEMU.

For advaced building instructions, check our build components reference [here](build-components.md) and our build options reference [here](build-options.md).
