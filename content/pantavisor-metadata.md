# Pantavisor Metadata

Check out our set device metadata [how to](set-device-metadata.md) guide if you don't know how to do it.

## SSH Authorized Keys

Prerequisites: create a SSH pub key in your host computer.

```
key: pvr-sdk.authorized_keys
value: <ssh pub id>
```

Expected result: SSH is now accesible from the host computer. More info [here](inspect-device.md).

## pvr Auto-Follow

Prerequisites: your device has our pvr-sdk app.

```
key: pvr-auto-follow.url
value: <pvr clone URL>
```

Expected result: the device will automatically pull every change in the device associated to that URL.
