
[TOC]

In order to begin using Pantavisor and Pantahub, please first prepare your working environment installing some dependencies and our pvr tool. Also, you will need to register in our web:

* [Prerequisites](environment-setup.md)

After that, you can follow this tutorial to get started on a Rapsberry Pi 3 (recommended for beginners):

* [Get started on Raspberry Pi 3](get-started.md)

If you are using other hardware, take a look at these how-to guides:

* [How to install images for other targets](choose-image.md)
* [Get started with LimeSDR on Raspberry Pi 3](limesdr-initial-devices.md)

Once your device is setup. You will be able to take advantage of Pantahub and Pantavisor to develop and deploy software on your device:

* [How to make changes to your device](clone-your-system.md)
* [How to build Pantavisor from source code](get-source-code.md)

For more in-depth documentation, have here our reference guides, explanations and other tutorials:

* [Pantahub API reference](https://docs.pantahub.com/pantahub-base/auth/#authenticate)
* [Pantavisor reference](build-options.md)

* [Technical deepdives](pantavisor-architecture.md)
* [Other turorials](mozilla-iot-gateway-as-a-pantavisor-app.md)
