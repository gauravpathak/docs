# Build Options

## Build debug system images

If you are developing pantavisor or some low level plumbing yourself you might need special facilities such as console output on tty, ssh, etc. that we disable in normal builds by default.

To enable debugging facilities you can pass the ```PANTAVISOR_DEBUG``` environment variable to the build. The Default is ```no```

```
PANTAVISOR_DEBUG=yes \
    PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable \
    ./build.docker.sh arm-rpi3
```

## Use auto-claim feature

The public pantavisor distribution supports to seed a shared secret (called 'autotok') during build that will make a device automatically join a user account of the operator.

Before you can build an image that has this token "built-in" you will have to acquire a token from pantahub. You can read how to do that in our [Pantahub API Reference](https://docs.pantahub.com/pantahub-base/devices/#auto-assign-devices-to-owners).

Not how you can not only acquire a token, but also specify default user-meta fields to seed on devices that join using that token. This will allow you to tag devices by product-id etc. for your management tooling etc.

Following that guideline you will be able to acquire a token. You now have to pass that token to the Pantavisor build system to produce a prebuilt image that will automatically join your account. To do so you use the ```PV_FACTORY_AUTOTOK``` environment variable when building the image:

```
PV_FACTORY_AUTOTOK=xxxxYOURTOKENxxxx \
    PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable \
    PANTAVISOR_DEBUG=yes \
    ./build.docker.sh arm-rpi3

```

## Non interactive build

This can be interesting for automated pantavisor build. By default build.docker.sh will ask for credentials when building against a pantahub URL. This can be avoided with the ```ACCESS_TOKEN``` and ```PV_BUILD_INTERACIVE``` environment variables.

First, you need to get a Pantahub token for your pvr credentials. You can read how to do this in our [Pantahub API Reference](https://docs.pantahub.com/pantahub-base/auth/#authenticate).

Now, you can use the previously mentioned environment variables along your newly generated token:

``` 
ACCESS_TOKEN=$TOKEN \
    PV_BUILD_INTERACIVE=false \
    PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable \
    ./build.docker.sh arm-rpi3
```
