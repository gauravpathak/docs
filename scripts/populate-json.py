#!/usr/bin/python

import sys
import boto3
import json
import dateutil.parser

def authenticate(access_key, secret_key):
    return boto3.client('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_key)

def collect_pipelines_info(session, bucket, path):
    paginator = session.get_paginator('list_objects_v2')
    pages = paginator.paginate(Bucket=bucket, Prefix=path)
    print("collect_pipelines_info: get objects from ", bucket, "/", path, "...")

    pipelines = {}
    for page in pages:
        print("collect_pipelines_info:", len(page['Contents']), "objects retrieved from page")
        for obj in page['Contents']:
            obj_list = obj['Key'].split('/')
            if len(obj_list) > 3:
                pipe = pipelines.setdefault(int(obj_list[2]), {'architectures':{},'metadata':None})
                pipe_file_name = obj_list[3]
                if pipe_file_name == "pipeline.json":
                    pipeline_metadata = session.get_object(Bucket='pantavisor-ci', Key=obj['Key'])
                    pipe['metadata'] = json.loads(pipeline_metadata['Body'].read().decode())
                    pipe['metadata']['path'] = obj_list[0] + "/" + obj_list[1]
                elif len(obj_list) > 4:
                    img_file_name = obj_list[4]
                    if img_file_name == "img.json":
                        img_metadata = session.get_object(Bucket='pantavisor-ci', Key=obj['Key'])
                        pipe['architectures'][pipe_file_name] = json.loads(img_metadata['Body'].read().decode())
    print("collect_pipelines_info:", len(pipelines), "pipelines retreived from objects")
    return pipelines

def get_latest_pipeline_success(pipelines):
    for pipe in sorted(pipelines.keys(), reverse=True):
        if 'status' in pipelines[pipe]['metadata'] and pipelines[pipe]['metadata']['status'] == "SUCCESS":
            return pipe
    return {}

def populate_download_images(pipelines):
    imgs_file = open('json/download-images.json', 'r')
    imgs_json = json.load(imgs_file)

    pipe = get_latest_pipeline_success(pipelines)
    print("populate_download_images: latest siccessful pipeline is", pipe)
    pipe_metadata = pipelines[pipe]['metadata']
    pipe_architectures = pipelines[pipe]['architectures']

    if pipe_metadata is not None:
        if 'pvmanifestversion' in pipe_metadata:
           	gitversion = pipe_metadata['pvmanifestversion']
        elif 'gitversion' in pipe_metadata:
            gitversion = pipe_metadata['gitversion']
        imgs_json[10]['ul'].append('Release: [%s](https://gitlab.com/pantacor/pv-manifest/commit/%s)' % (gitversion,pipe_metadata['gitsha']))
        date = dateutil.parser.parse(pipe_metadata['committime'])
        imgs_json[10]['ul'].append('Date: %s %d, %d' % (date.strftime('%B'),date.day,date.year))
        imgs_json[10]['ul'].append('PVR version: %s' % (pipe_metadata['pvrversion'].split(' ')[2]))

        for arch in pipe_architectures:
            arch_metadata = pipe_architectures[arch]
            arch_row = []
            arch_row.append('**%s**' % (arch))
            arch_row.append('%s' % (arch_metadata['target']))
            if arch_metadata['image'] == "no":
                arch_row.append(' ')
            else:
                arch_row.append('[download](https://pantavisor-ci.s3.amazonaws.com/%s/%s/%s/%s.img.xz)' % (pipe_metadata['path'],pipe,arch,arch))
            if arch_metadata['installer'] == "yes":
                arch_row.append(' ')
            else:
                arch_row.append('[device](https://www.pantahub.com/u/pantahub-ci/devices/%s/step/%s)' % (arch_metadata['deviceid'],arch_metadata['pantahubrevision']))
            imgs_json[12]['table']['rows'].append(arch_row)

    imgs_file.close()
    return imgs_json

def populate_pantavisor_ci_table(pipelines):
    ci_file = open('json/pantavisor-ci-table.json', 'r')
    ci_json = json.load(ci_file)

    print("populate_pantavisor_ci_table: the number of pinelines in the table is", len(pipelines))

    for pipe in sorted(pipelines.keys(), reverse=True):
        pipe_metadata = pipelines[pipe]['metadata']
        pipe_architectures = pipelines[pipe]['architectures']
        if pipe_metadata is not None:
            pipe_row = []
            if 'pvmanifestversion' in pipe_metadata:
               	gitversion = pipe_metadata['pvmanifestversion']
            elif 'gitversion' in pipe_metadata:
                gitversion = pipe_metadata['gitversion']
            pipe_row.append('[%s](https://gitlab.com/pantacor/pv-manifest/commit/%s)' % (gitversion,pipe_metadata['gitsha']))
            pipe_row.append('%s' % (pipe_metadata['committime']))
            pipe_row.append('%s' % (pipe_metadata['pvrversion'].split(' ')[2]))

            images=''
            for arch in pipe_architectures:
                arch_metadata = pipe_architectures[arch]
                if "image" not in arch_metadata or arch_metadata['image'] != "no":
                    images+='[%s](https://pantavisor-ci.s3.amazonaws.com/%s/%s/%s/%s.img.xz) \| ' % (arch,pipe_metadata['path'],pipe,arch,arch)
            pipe_row.append(images[:-4])

            devices=''
            for arch in pipe_architectures:
                arch_metadata = pipe_architectures[arch]
                if "installer" not in arch_metadata or arch_metadata['installer'] != "yes":
                    devices+='[%s](https://www.pantahub.com/u/pantahub-ci/devices/%s/step/%s) \| ' % (arch,arch_metadata['deviceid'],arch_metadata['pantahubrevision'])
            pipe_row.append(devices[:-4])

            pipe_row.append('[%s](https://gitlab.com/pantacor/pv-manifest/pipelines/%s)' % (pipe_metadata['status'],pipe))
            ci_json[2]['table']['rows'].append(pipe_row)

    ci_file.close()
    return ci_json

def write_output_download_images(imgs_json,ci_json):
    imgs_file = open('download-images.json', 'w')
    imgs_file.write(json.dumps(imgs_json))
    imgs_file.close()

    ci_file = open('pantavisor-ci-table.json', 'w')
    ci_file.write(json.dumps(ci_json))
    ci_file.close()

if len(sys.argv) == 3:
    s3 = authenticate(sys.argv[1], sys.argv[2])

    pipelines = {}
    pipelines.update(collect_pipelines_info(s3, 'pantavisor-ci', 'branches/master/'))
    pipelines.update(collect_pipelines_info(s3, 'pantavisor-ci', 'tags/'))

    imgs_json = populate_download_images(pipelines)
    ci_json = populate_pantavisor_ci_table(pipelines)

    write_output_download_images(imgs_json,ci_json)
