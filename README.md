# README

## How to locally test changes in this repo

To check how your changes in docs look like, you can use docker:

```
docker build -f Dockerfile -t docs .
docker run -p 8080:80 docs
```

A nginx instance is now running locally. In your web browser, go to http://localhost:8080/ to acess the built docs.
