FROM alpine:3.10 as builder

WORKDIR /work

RUN apk update && apk add gcc g++ python3 python3-dev linux-headers
RUN pip3 install --upgrade pip \
		&& pip3 install Markdown mkdocs mkdocs-material mkdocs-redirects

COPY . .
RUN mkdocs build

FROM nginx

COPY --from=builder /work/site /usr/share/nginx/html
